using UnityEngine;

public class Core : MonoBehaviour
{
    [SerializeField] private EnemyManager spawner;

    private void OnTriggerEnter(Collider other)
    {
        spawner.Despawn(other);
    }
}
