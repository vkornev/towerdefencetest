using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    [SerializeField] private bool automaticSpawn;
    [SerializeField] private float spawnDelay;
    [SerializeField] private Transform defaultSpawnPoint;
    [SerializeField] private Transform finish;
    [SerializeField] private float runnerHealth;
    [SerializeField] private string runnerPrefabLink = "Prefabs/Unit";
    [SerializeField] private ReplayManager replayManager;

    private Queue<Runner> unitPool;
    private float timer;
    private Dictionary<Collider, Runner> spawnedUnits;

    private ReplayStatus replayStatus;

    public void Spawn(Vector3 position, Quaternion rotation)
    {
        timer = spawnDelay;

        Runner unit;
        if (unitPool.Count == 0)
            RequestCreateUnit();

        unit = unitPool.Dequeue();
        if (unit.IsReady)
            InsertUnit(unit, position, rotation);
        else
        {
            unit.OnReady += () =>
            {
                InsertUnit(unit, position, rotation);
            };
        }
    }

    public void Despawn(Collider unitCollider)
    {
        if (!spawnedUnits.ContainsKey(unitCollider))
            return;

        Runner unit = spawnedUnits[unitCollider];
        unit.Deactivate();
        spawnedUnits.Remove(unitCollider);
        unitPool.Enqueue(unit);
    }

    public Runner GetRunner(Collider collider)
    {
        if (!spawnedUnits.ContainsKey(collider))
            return null;

        return spawnedUnits[collider];
    }

    private void Awake()
    {
        unitPool = new Queue<Runner>();
        spawnedUnits = new Dictionary<Collider, Runner>();
        timer = 0f;
        replayStatus = ReplayStatus.Record;
    }

    private void OnEnable()
    {
        replayManager.OnRewind += SetRewind;
        replayManager.OnReplay += SetReplay;
        replayManager.OnPause += SetPause;
    }

    private void OnDisable()
    {
        replayManager.OnRewind -= SetRewind;
        replayManager.OnReplay -= SetReplay;
        replayManager.OnPause -= SetPause;
    }

    private void Update()
    {
        if (!automaticSpawn || replayStatus != ReplayStatus.Record)
            return;

        timer -= Time.deltaTime;

        if (timer <= 0f)
            Spawn(defaultSpawnPoint.position, defaultSpawnPoint.rotation);
    }

    private void RequestCreateUnit()
    {
        Runner unit = new Runner(runnerPrefabLink, runnerHealth);
        replayManager.AddObject(unit);
        unitPool.Enqueue(unit);
    }

    private void InsertUnit(Runner unit, Vector3 position, Quaternion rotation)
    {
        unit.Activate(position, rotation);
        unit.DirectTo(finish.position);
        unit.OnDeath += RunnerDeath;
        spawnedUnits.Add(unit.ColliderObj, unit);
    }

    private void RunnerDeath(Runner runner)
    {
        runner.OnDeath -= RunnerDeath;
        Despawn(runner.ColliderObj);
    }

    private void SetRewind()
    {
        replayStatus = ReplayStatus.Rewind;
    }

    private void SetReplay()
    {
        replayStatus = ReplayStatus.Replay;
    }

    private void SetPause()
    {
        replayStatus = ReplayStatus.Pause;
    }
}
