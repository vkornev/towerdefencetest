using System;
using UnityEngine;

public interface IPoolable
{
    public Action OnReady { get; set; }
    public bool IsReady { get; }
    public bool IsActive { get; }
    public Transform TransformObj { get; }
    public Collider ColliderObj { get; }

    public void Activate(Vector3 position, Quaternion rotation);
    public void Deactivate();
}
