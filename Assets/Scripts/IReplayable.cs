using UnityEngine;

public enum ReplayStatus { Record, Rewind, Replay, Pause }

public class SavedObject
{
    public IReplayable saveObject;
    public Vector3 position;
    public Quaternion rotation;
    public bool isActive;
}

public interface IReplayable
{
    void SetReplayStatus(ReplayStatus status);
    SavedObject GetSaveData();
    void SetSaveData(SavedObject data);
}
