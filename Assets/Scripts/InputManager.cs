﻿using System;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    public static event Action<Vector3> OnClick;
    public static event Action OnSpaceDown;
    public static event Action OnSpaceUp;

    private void Update()
    {
        if (Input.GetMouseButtonUp(0))
            OnClick?.Invoke(Input.mousePosition);

        if (Input.GetKeyDown(KeyCode.Space))
            OnSpaceDown?.Invoke();

        if (Input.GetKeyUp(KeyCode.Space))
            OnSpaceUp?.Invoke();
    }
}
