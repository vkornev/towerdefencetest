using UnityEngine;

public class ManualSpawner : MonoBehaviour
{
    [SerializeField] private EnemyManager enemyManager;
    [SerializeField] private Camera mainCamera;

    private RaycastHit[] hits;

    private void Awake()
    {
        hits = new RaycastHit[1];
    }

    private void OnEnable()
    {
        InputManager.OnClick += SpawnRunner;
    }

    private void OnDisable()
    {
        InputManager.OnClick -= SpawnRunner;
    }

    private void SpawnRunner(Vector3 position)
    {
        Ray ray = mainCamera.ScreenPointToRay(position);
        int count = Physics.RaycastNonAlloc(ray, hits, mainCamera.farClipPlane);

        if (count == 0) return;

        enemyManager.Spawn(hits[0].point, Quaternion.identity);
    }
}
