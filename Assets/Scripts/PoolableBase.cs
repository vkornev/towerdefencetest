using System;
using UnityEngine;

public abstract class PoolableBase : IPoolable, IReplayable
{
    public Action OnReady { get; set; }
    public bool IsReady { get; protected set; }
    public bool IsActive { get; protected set; }
    public Collider ColliderObj { get; protected set; }
    public Transform TransformObj { get; protected set; }

    protected GameObject gameObject;
    protected ReplayStatus replayStatus;

    public PoolableBase(string prefabLink)
    {
        replayStatus = ReplayStatus.Record;
        IsReady = false;
        IsActive = false;
        ResourceRequest request = Resources.LoadAsync(prefabLink);
        request.completed += (asyncOp) =>
        {
            FinishedLoadingAsset(request);
            IsReady = true;
            OnReady?.Invoke();
        };
    }

    public virtual void Activate(Vector3 position, Quaternion rotation)
    {
        if (!gameObject)
            return;

        TransformObj.position = position;
        TransformObj.rotation = rotation;
        gameObject.SetActive(true);
        IsActive = true;
    }

    public virtual void Deactivate()
    {
        IsActive = false;
        gameObject.SetActive(false);
        TransformObj.position = Vector3.zero;
        TransformObj.rotation = Quaternion.identity;
    }

    protected virtual void FinishedLoadingAsset(ResourceRequest request)
    {
        if (!request.isDone)
        {
            Debug.LogError("Unable to load the unit");
            return;
        }

        GameObject prefab = (GameObject)request.asset;
        if (!prefab)
        {
            Debug.LogError("Unit loaded as null");
            return;
        }

        gameObject = GameObject.Instantiate(prefab, Vector3.zero, Quaternion.identity);
        TransformObj = gameObject.transform;

        if (gameObject.TryGetComponent<Collider>(out Collider collider))
            ColliderObj = collider;
    }

    public virtual void SetReplayStatus(ReplayStatus status)
    {
    }

    public SavedObject GetSaveData()
    {
        return new SavedObject
        {
            isActive = IsActive,
            position = TransformObj.position,
            rotation = TransformObj.rotation,
            saveObject = this
        };
    }

    public void SetSaveData(SavedObject data)
    {
        if (data == null)
        {
            IsActive = false;
            gameObject.SetActive(false);
            return;
        }

        IsActive = data.isActive;
        gameObject.SetActive(data.isActive);
        TransformObj.position = data.position;
        TransformObj.rotation = data.rotation;
    }
}
