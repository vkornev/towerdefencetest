using System;
using UnityEngine;

public class Projectile : PoolableBase
{
    public event Action<Projectile, Runner> OnTargetHit;

    private const float minDistance = .25f;

    private Runner target;
    private float speed;
    private float damage;

    public Projectile(string prefabLink, float speed) : base(prefabLink)
    {
        this.speed = speed;
    }

    public void Init(Runner target, float damage)
    {
        this.target = target;
        this.damage = damage;
    }

    public void Move()
    {
        if (replayStatus != ReplayStatus.Record)
            return;

        if (target == null || !target.IsActive)
        {
            OnTargetHit?.Invoke(this, null);
            return;
        }
        Vector3 normalized = (target.TransformObj.position - TransformObj.position).normalized;
        Vector3 normDir = Vector3.Lerp(TransformObj.forward, normalized, Time.deltaTime * speed * 2f);

        TransformObj.forward = normDir;
        TransformObj.position += normDir * speed * Time.deltaTime;
        if ((target.TransformObj.position - TransformObj.position).magnitude <= minDistance)
        {
            target.Damage(damage);
            OnTargetHit?.Invoke(this, target);
        }
    }

    public override void SetReplayStatus(ReplayStatus status)
    {
        replayStatus = status;
    }
}