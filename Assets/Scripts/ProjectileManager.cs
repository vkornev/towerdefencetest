using System.Collections.Generic;
using UnityEngine;

public class ProjectileManager : MonoBehaviour
{
    public struct ProjectileData
    {
        public string prefabLink;
        public Runner target;
        public Vector3 position;
        public Quaternion rotation;
        public float speed;
        public float damage;
    }

    [SerializeField] private ReplayManager replayManager;

    private Queue<Projectile> projectilesPool = new Queue<Projectile>();
    private List<Projectile> allProjectiles = new List<Projectile>();

    private void Update()
    {
        foreach (Projectile projectile in allProjectiles)
            if (projectile.IsActive)
                projectile.Move();
    }

    public void RequestLaunchProjectile(ProjectileData data)
    {
        if (projectilesPool.Count == 0)
        {
            Projectile newProjectile = new Projectile(data.prefabLink, data.speed);
            projectilesPool.Enqueue(newProjectile);
            allProjectiles.Add(newProjectile);
            replayManager.AddObject(newProjectile);
        }

        Projectile projectile = projectilesPool.Dequeue();
        if (projectile.IsReady)
            InitProjectile(data, projectile);
        else
            projectile.OnReady += () => { InitProjectile(data, projectile); };
    }

    private void InitProjectile(ProjectileData data, Projectile projectile)
    {
        projectile.Init(data.target, data.damage);
        projectile.Activate(data.position, data.rotation);
        projectile.OnTargetHit += TargetHit;
    }

    private void TargetHit(Projectile projectile, Runner target)
    {
        projectile.OnTargetHit -= TargetHit;
        projectile.Deactivate();
        projectilesPool.Enqueue(projectile);
    }
}
