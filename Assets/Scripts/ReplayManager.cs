using System;
using System.Collections.Generic;
using UnityEngine;

public class SavedFrame
{
    public List<SavedObject> activeObjects;
}

public class ReplayManager : MonoBehaviour
{
    public event Action OnRewind;
    public event Action OnReplay;
    public event Action OnPause;
    public event Action<float> OnReplayProgressChange;

    [SerializeField] private ReplayUI replayUI;

    private List<SavedFrame> frames;
    private List<IReplayable> watchList;
    private ReplayStatus replayStatus;

    private int currentFrame;

    public void AddObject(IReplayable obj)
    {
        watchList.Add(obj);
    }

    private void Awake()
    {
        watchList = new List<IReplayable>();
        frames = new List<SavedFrame>();
        replayStatus = ReplayStatus.Record;
        currentFrame = 0;
    }

    private void OnEnable()
    {
        InputManager.OnSpaceDown += SetRewind;
        InputManager.OnSpaceUp += SetReplay;

        replayUI.OnPlay += SetReplay;
        replayUI.OnPause += SetPause;
        replayUI.OnRewind += SetRewind;
        replayUI.OnUIReplayProgressChange += SetProgressFrame;
    }

    private void OnDisable()
    {
        InputManager.OnSpaceDown -= SetRewind;
        InputManager.OnSpaceUp -= SetReplay;

        replayUI.OnPlay -= SetReplay;
        replayUI.OnPause -= SetPause;
        replayUI.OnRewind -= SetRewind;
        replayUI.OnUIReplayProgressChange -= SetProgressFrame;
    }

    private void FixedUpdate()
    {
        switch (replayStatus)
        {
            case ReplayStatus.Record:
                Record();
                break;
            case ReplayStatus.Rewind:
                Rewind();
                break;
            case ReplayStatus.Replay:
                Replay();
                break;
            default:
                break;
        }
    }

    private void Record()
    {
        SavedFrame frame = new SavedFrame();
        frame.activeObjects = new List<SavedObject>();
        for (int i = 0; i < watchList.Count; i++)
        {
            SavedObject obj = watchList[i].GetSaveData();
            frame.activeObjects.Add(obj);
        }

        frames.Add(frame);
    }

    private void Rewind()
    {
        PlayFrame();

        currentFrame--;
    }

    private void Replay()
    {
        PlayFrame();

        currentFrame++;
    }

    private void SetRewind()
    {
        replayStatus = ReplayStatus.Rewind;
        currentFrame = frames.Count - 1;

        for (int i = 0; i < watchList.Count; i++)
            watchList[i].SetReplayStatus(ReplayStatus.Rewind);

        OnRewind?.Invoke();
    }

    private void SetReplay()
    {
        replayStatus = ReplayStatus.Replay;

        for (int i = 0; i < watchList.Count; i++)
            watchList[i].SetReplayStatus(ReplayStatus.Replay);

        OnReplay?.Invoke();
    }

    private void SetPause()
    {
        replayStatus = ReplayStatus.Pause;

        for (int i = 0; i < watchList.Count; i++)
            watchList[i].SetReplayStatus(ReplayStatus.Pause);

        OnPause?.Invoke();
    }

    private void SetProgressFrame(float progress)
    {
        currentFrame = (int)(frames.Count * progress);

        PlayFrame();
    }

    private void PlayFrame()
    {
        if (currentFrame < 0 || currentFrame >= frames.Count)
            return;

        SavedFrame frame = frames[currentFrame];

        // this list contains only objects that don't exist in the current frame
        List<IReplayable> leftOvers = new List<IReplayable>(watchList);
        for (int i = 0; i < frame.activeObjects.Count; i++)
        {
            IReplayable obj = frame.activeObjects[i].saveObject;

            leftOvers.Remove(obj);

            obj.SetSaveData(frame.activeObjects[i]);
        }

        // now we need to hide all "non-existent" objects
        for (int i = 0; i < leftOvers.Count; i++)
        {
            leftOvers[i].SetSaveData(null);
        }

        OnReplayProgressChange?.Invoke((float)currentFrame / frames.Count);
    }
}