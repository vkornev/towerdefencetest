using System;
using UnityEngine;
using UnityEngine.AI;

public class Runner : PoolableBase
{
    public event Action<Runner> OnDeath;

    public float Health { get; private set; }

    private NavMeshAgent navAgent;
    private float maxHealth;

    public Runner(string prefabLink, float maxHealth) : base(prefabLink)
    {
        this.maxHealth = maxHealth;
    }

    public override void Activate(Vector3 position, Quaternion rotation)
    {
        if (replayStatus != ReplayStatus.Record)
            return;

        base.Activate(position, rotation);
        navAgent.Warp(position);
        Health = maxHealth;
    }

    public void DirectTo(Vector3 position)
    {
        if (replayStatus != ReplayStatus.Record)
            return;

        navAgent.SetDestination(position);
    }

    public void Damage(float damage)
    {
        Health -= damage;

        if (Health <= 0f)
            OnDeath?.Invoke(this);
    }

    public override void SetReplayStatus(ReplayStatus status)
    {
        replayStatus = status;
        if (navAgent.gameObject.activeInHierarchy)
            navAgent.isStopped = replayStatus != ReplayStatus.Record;
    }

    protected override void FinishedLoadingAsset(ResourceRequest request)
    {
        base.FinishedLoadingAsset(request);

        if (!gameObject)
            return;
        navAgent = gameObject.GetComponent<NavMeshAgent>();
    }
}
