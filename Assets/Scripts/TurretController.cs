﻿using System.Collections.Generic;
using UnityEngine;

public class TurretController : MonoBehaviour, IReplayable
{
    private const string projectilePrefabLink = "Prefabs/Projectile";

    [SerializeField] private ProjectileManager projectileManager;
    [SerializeField] private EnemyManager enemyManager;
    [SerializeField] private ReplayManager replayManager;
    [SerializeField] private Transform turret;
    [SerializeField] private Transform barrel;
    [SerializeField] private Transform spawnLocation;

    [SerializeField] private float fireDelay;
    [SerializeField] private float damage;
    [Range(0f, 90f)]
    [SerializeField] private float barrelAngle;
    [SerializeField] private float projectileSpeed;

    private float timer = 0f;
    private List<Runner> unitsInRange = new List<Runner>();
    private Runner target = null;

    private ReplayStatus replayStatus;

    private ProjectileManager.ProjectileData projectileData;

    public void SetReplayStatus(ReplayStatus status)
    {
        this.replayStatus = status;
    }

    public SavedObject GetSaveData()
    {
        return new SavedObject
        {
            isActive = true,
            position = turret.position,
            rotation = turret.rotation,
            saveObject = this
        };
    }

    public void SetSaveData(SavedObject data)
    {
        turret.rotation = data.rotation;
    }

    private void Awake()
    {
        replayStatus = ReplayStatus.Record;
    }

    private void Start()
    {
        projectileData = new ProjectileManager.ProjectileData
        {
            target = target,
            prefabLink = projectilePrefabLink,
            position = spawnLocation.position,
            rotation = spawnLocation.rotation,
            speed = projectileSpeed,
            damage = damage
        };

        replayManager.AddObject(this);
        // I might understand wrong the request of editing the horizontal barrel angle
        barrel.eulerAngles = new Vector3(-barrelAngle, 0f, 0f);
    }

    private void Update()
    {
        if (replayStatus != ReplayStatus.Record)
            return;

        timer -= Time.deltaTime;

        AdjustTurretRotation();

        if (target != null && target.IsActive && timer <= 0f)
        {
            Shoot();
        }
        else if (target == null && unitsInRange.Count > 0)
        {
            target = unitsInRange[0];
            unitsInRange.RemoveAt(0);
            target.OnDeath += OnRunnerDeath;
        }
    }

    private void Shoot()
    {
        projectileManager.RequestLaunchProjectile(projectileData);
        timer = fireDelay;
    }

    private void OnTriggerEnter(Collider other)
    {
        Runner runner = enemyManager.GetRunner(other);

        if (runner == null)
            return;

        unitsInRange.Add(runner);
    }

    private void OnTriggerExit(Collider other)
    {
        Runner quited = null;

        if (target == null)
            return;

        if (target.ColliderObj == other)
        {
            target.OnDeath -= OnRunnerDeath;
            target = null;
            return;
        }

        foreach (Runner unit in unitsInRange)
        {
            if (unit.ColliderObj != other)
                continue;

            quited = unit;
            break;
        }

        if (quited != null)
            unitsInRange.Remove(quited);
    }

    private void OnRunnerDeath(Runner runner)
    {
        runner.OnDeath -= OnRunnerDeath;
        if (runner == target)
            target = null;
    }

    private void AdjustTurretRotation()
    {
        if (target == null)
            return;

        turret.LookAt(target.TransformObj.position);
        Vector3 euler = turret.eulerAngles;
        euler.x = 0f;
        euler.z = 0f;
        turret.eulerAngles = euler;
    }
}
