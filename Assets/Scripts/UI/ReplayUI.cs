using System;
using UnityEngine;
using UnityEngine.UI;

public class ReplayUI : MonoBehaviour
{
    public event Action OnRewind;
    public event Action OnPause;
    public event Action OnPlay;
    public event Action<float> OnUIReplayProgressChange;

    [SerializeField] private ReplayManager replayManager;
    [SerializeField] private Button pause;
    [SerializeField] private Button play;
    [SerializeField] private Button rewind;
    [SerializeField] private Slider progressBar;

    private void OnEnable()
    {
        pause.onClick.AddListener(PauseClicked);
        play.onClick.AddListener(PlayClicked);
        rewind.onClick.AddListener(RewindClicked);
        progressBar.onValueChanged.AddListener(ProgressChanged);
        replayManager.OnReplayProgressChange += UpdateProgressBar;
    }

    private void OnDisable()
    {
        pause.onClick.RemoveListener(PauseClicked);
        play.onClick.RemoveListener(PlayClicked);
        rewind.onClick.RemoveListener(RewindClicked);
        progressBar.onValueChanged.RemoveListener(ProgressChanged);
        replayManager.OnReplayProgressChange -= UpdateProgressBar;
    }

    private void PlayClicked()
    {
        OnPlay?.Invoke();
    }

    private void PauseClicked()
    {
        OnPause?.Invoke();
    }

    private void RewindClicked()
    {
        OnRewind?.Invoke();
    }

    private void ProgressChanged(float val)
    {
        OnUIReplayProgressChange?.Invoke(val);
    }

    private void UpdateProgressBar(float val)
    {
        progressBar.SetValueWithoutNotify(val);
    }
}
